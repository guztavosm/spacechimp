#Layers
0 - Bg layer
	- BG
1 - Game layer
	- Bg Layer
	- Score Layer
	- Player
2 - Score layer
	- Score Label
	- Lifes Label
	- Gas bar

#Elementos voladores
1 - Vida
2 - Gas
3 - Meteorito verde
4 - Meteorito amarillo 
5 - Meteorito rojo
6 - Meteorito normal	
	
#Efectos de sonido
- Morir
- Agarrar un item
- Se esta acabando el gas
- Volar

#Efectos gas bajo
- Cambia el sprite
- Se vuelve mas lento (cambia gravedad)