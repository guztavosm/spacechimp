var flyingElm = cc.Sprite.extend({
	// Properties
	elmType : 0,
	
	initPosition: null,
	endPosition: null,
	doMove: true,
	action: null,
	
	lvlConfig: null,
	winSize: null,
	
	// Functions
	ctor : function(elmType, lvlConfig){
		this._super();
		
		this.winSize = cc.winSize;
		this.elmType = elmType;
		this.lvlConfig = lvlConfig;
		switch(elmType){
			case 1:
				this.initVida();
				break;
			case 2:
				this.initGas();
				break;
			case 3:
				this.initMetVerde();
				break;
			case 4:
				this.initMetAmarillo();
				break;
			case 5:
				this.initMetRojo();
				break;
			default:
				this.initMeteorito();
				break;
			
		}	
	},
	onEnter : function(){
		this._super(); 
		this.setPosition(this.initPosition);
		
		if(this.doMove){
			var moveAction = new cc.MoveTo(this.lvlConfig.SPEED, this.endPosition);
			this.runAction(moveAction);
		}
			
		this.runAction(this.action);
		this.scheduleUpdate();
	},
	update : function(){
		var parent = this.getParent();
		var playerBoundingBox = parent.player.getChimpBox();
		var itemBoundingBox = this.getBoundingBox();
		if(cc.rectIntersectsRect(playerBoundingBox, itemBoundingBox)){
			parent.playerHit(this);
		} 
		
		if(this.getPosition().x<-50){
			parent.removeObject(this);
		}	
	},
	// Init functions
	initMeteorito : function(){
		this.initWithFile(res.met_png);
		this.setScale(0.05); // Escala provisional
		
		this.initPosition = cc.p(this.winSize.width, Math.random() * this.winSize.height);
		this.endPosition = cc.p(-100, Math.random() * this.winSize.height);
		
		this.action = new cc.RepeatForever(new cc.RotateBy(5, 360));
	},
	initVida : function(){
		this.initWithFile(res.vida_png);
		this.setScale(0.06); // Escala provisional
		
		var Y = Math.random() * this.winSize.height;
		
		this.initPosition = cc.p(this.winSize.width, Y);
		this.endPosition = cc.p(-100, Y);
		
		// Configure scaling animation
		var scale = 1.5;
		var scaleUp = new cc.ScaleBy(0.4, scale);
		var scaleDown = new cc.ScaleBy(0.4, (1/scale));
		var sequence = new cc.Sequence(scaleUp, scaleDown);
		this.action = new cc.RepeatForever(sequence);
	},
	initGas : function(){
		this.initWithFile(res.gasolina_png);
		this.doMove = false;
		this.setScale(0.06); // Escala provisional
		
		var Y = Math.random() * (this.winSize.height - 60);
		
		this.initPosition = cc.p(this.winSize.width, Y);
		this.endPosition = cc.p(-100, Y);
		
		// Configure scaling animation
		this.action = new cc.JumpTo(this.lvlConfig.SPEED, this.endPosition, 60, 2);
	},
	initMetVerde : function(){
		this.initWithFile(res.metverde_png);
		this.setScale(0.06); // Escala provisional
		
		var Y = Math.random() * this.winSize.height;
		
		this.initPosition = cc.p(this.winSize.width, Y);
		this.endPosition = cc.p(-100, Y);
		
		this.action = new cc.RepeatForever(new cc.RotateBy(5, 360));
	},
	initMetAmarillo : function(){
		this.initWithFile(res.metamarillo_png);
		this.setScale(0.06); // Escala provisional
		
		var Y = Math.random() * this.winSize.height;
		
		this.initPosition = cc.p(this.winSize.width, Y);
		this.endPosition = cc.p(-100, Y);
		
		this.action = new cc.RepeatForever(new cc.RotateBy(5, 360));
	},
	initMetRojo : function(){
		this.initWithFile(res.metrojo_png);
		this.setScale(0.06); // Escala provisional
		
		var Y = Math.random() * this.winSize.height;
		
		this.initPosition = cc.p(this.winSize.width, Y);
		this.endPosition = cc.p(-100, Y);
		
		this.action = new cc.RepeatForever(new cc.RotateBy(4, 360));
	}
});