var gasBar = cc.Sprite.extend({
	// Properties
	blinkAction: null,
	
	// Functions
	ctor: function(){
		this._super(res.gas7_png);
		this.setScale(0.15);
	},
	replaceGas : function(level){
		var file = res.gas7_png;
		file = file.replace("7", level);
		this.initWithFile(file);
		
		if(this.blinkAction){
			this.stopAction(this.blinkAction);
			this.setVisible(true);
		}
		
		if(level <= CONST.LOWGASLEVEL){
			if(level == 2)
				this.blinkAction = new cc.RepeatForever(new cc.Blink(1, 4));
			else 
				this.blinkAction = new cc.RepeatForever(new cc.Blink(1, 8));
				
			this.runAction(this.blinkAction);
		}
	},
	restart : function(){
		this.replaceGas(7);
	}
});