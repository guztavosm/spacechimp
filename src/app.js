var GameLayer = cc.Layer.extend({
	// Properties
	bglayer : null,
	scoreLayer : null,
	player : null,
	lvlConfig: null,
	
	level: 1,
	winSize : null,
	
	lifes: 5,
	score: 0,
	
	drawer: null,
	
	// Functions
    ctor:function () {
        this._super();
        // Add and configure Score Layer
        this.scoreLayer = new ScoreLayer(),
		this.addChild(this.scoreLayer, 3);
		
		// Add and configure Bg Layer
		this.bglayer = new BackgroundLayer();
		this.addChild(this.bglayer, 1);
		
		// Add and configure Player
		this.player = new Player();
		this.addChild(this.player, 2);
	    
	    // Configure events touch and mouse
	    if ('mouse' in cc.sys.capabilities)
            cc.eventManager.addListener({
                event: cc.EventListener.MOUSE,
                onMouseDown: function (event) {
                    if (event.getButton() == cc.EventMouse.BUTTON_LEFT)
                        event.getCurrentTarget().processTouch(event);
                },
                onMouseUp: function(event){
	                event.getCurrentTarget().processTouchEnd(event);
                }
            }, this);

        if ('touches' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                prevTouchId: -1,
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                onTouchBegan: function(touch, event) {
	                event.getCurrentTarget().processTouch(touch);
                    return true;
                },
                onTouchEnded: function(touch, event){
	                event.getCurrentTarget().processTouchEnd(touch);
                }
            }, this);
        }
        
        if('keyboard' in cc.sys.capabilities){
	        cc.eventManager.addListener({
			    event: cc.EventListener.KEYBOARD,
			    onKeyPressed:  function(keyCode, event){
				  	if(keyCode == 32) 
				  		event.getCurrentTarget().processTouch(event);
			    },
			    onKeyReleased: function(keyCode, event){
				    if(keyCode == 32) 
				    	event.getCurrentTarget().processTouchEnd(event);
			    }
		    }, this);
        }
        
        // Initialize level
		this.init();
		this.drawer = new cc.DrawNode();
		this.addChild(this.drawer, 5);
		
        return true;  
    },
    init : function(){
	    this.winSize = cc.winSize;
	    
	    // Get level configuration
	    switch(this.level){
		    case 1:
		    	this.lvlConfig = LEVEL_CONFIG.LVL1;
		    	break;
		    case 2:
		    	this.lvlConfig = LEVEL_CONFIG.LVL2;
		    	break;
		    case 3:
		    	this.lvlConfig = LEVEL_CONFIG.LVL3;
		    	break;
		    case 4:
		    	this.lvlConfig = LEVEL_CONFIG.LVL4;
		    	break;
	    }
        
        
        // Configure timing functions
        this.scheduleUpdate();
        this.restartGasTiming();
        this.schedule(this.scoreUpdate, 1);
        this.schedule(this.spawnAsteroid, this.lvlConfig.SPAWNTIME);
        this.schedule(this.spawnGas, this.lvlConfig.SPAWNGASTIME);
        
    },
    update:function(dt){
		this.player.updateY();	
		this.bglayer.scroll();	
	},
    processTouch : function(event){
	    if(!cc.director.isPaused()){
		    this.player.engineOn = true;
        }
    },
    processTouchEnd : function(event){
	    if(!cc.director.isPaused()){
	        this.player.engineOn = false;
        }
    },
    
    restartGasTiming: function(){
	    this.unschedule(this.lessGas);
		this.schedule(this.lessGas, this.lvlConfig.REDUCEGASTIME);
    },
    
    // Timing functions
    lessGas : function(){
	    var gasLevel = this.player.reduceGas();
	    this.scoreLayer.setGasLevel(gasLevel);
	    if(gasLevel == 0)
	    	this.playerDead();
    },
    
    
    scoreUpdate : function(){
	    this.score += 10;
	    this.scoreLayer.setScore(this.score);
    },
    
    spawnAsteroid : function(){
	    // Creating the spawn elements in this level
	    var num = this.getRandomInt(this.lvlConfig.ASTEROIDMIN, this.lvlConfig.ASTEROIDMAX);
	    this.addObject(num);
	    for(var i = 1; i < this.lvlConfig.SPAWNOBJ; i++){
		    var num = this.getRandomInt(this.lvlConfig.ASTEROIDMIN, this.lvlConfig.ASTEROIDMAX);
		    this.scheduleOnce(function(){
			    this.addObject(num);
		    }, this.lvlConfig.DELAYSPWN * i);
	    }
    },
    
    spawnGas : function(){
	    this.addObject(FLYING_OBJECTS.GAS);
    },
    
    // Player functions
    playerDead : function(){
	    this.lifes--;
	    this.scoreLayer.setLifes(this.lifes);

	    this.player.die();
	    this.restartGasTiming();
	    this.scoreLayer.gasRestart();
    },
    playerHit : function(obj){
	    // Validation variables
	    var isInv = this.player.state == PLAYER_STATE.INVUNERABLE;
	    var isMet = FLYING_OBJECTS.METEOR.indexOf(obj.elmType) >= 0 || 
	    	obj.elmType == FLYING_OBJECTS.GREENMETEOR || 
	    	obj.elmType == FLYING_OBJECTS.YELLOWMETEOR;
	    var isRed = obj.elmType == FLYING_OBJECTS.REDMETEOR;
	    if(!isInv || (isInv && !isMet && !isRed)){
	    	var playerBox = this.player.getBoundingBox();
		 	var objBox = obj.getBoundingBox();
		 	var blanco = cc.Color(255,255,255, 0);
		 	
		 	//this.drawer.clear();
		 	//this.drawer.drawRect(cc.p(playerBox.x,playerBox.y),cc.p(playerBox.x + playerBox.width, playerBox.y + playerBox.height), blanco);
		 	//this.drawer.drawRect(cc.p(objBox.x,objBox.y),cc.p(objBox.x + objBox.width, objBox.y + objBox.height), blanco);
		 	
		 	switch(obj.elmType){
			 	case FLYING_OBJECTS.LIFE:
			 		this.playerHitLife();
			 		break;
			 	case FLYING_OBJECTS.GAS:
			 		this.playerHitGas();
			 		break;
			 	case FLYING_OBJECTS.GREENMETEOR:
			 		this.playerHitGreenMeteor();
			 		break;
			 	default:
			 		this.playerHitMeteor();
			 		break;
			}
			this.removeObject(obj);   
	    }
    },
    
    //Player Hit functions
    playerHitLife : function(){
	    this.lifes++;
	    this.scoreLayer.setLifes(this.lifes);
    },
    playerHitGas : function(){
	    var gasLevel = this.player.increaseGas();
		this.restartGasTiming();
	    this.scoreLayer.setGasLevel(gasLevel);
    },
    playerHitGreenMeteor : function(){
		this.player.big();
    },
    playerHitMeteor: function(){
	    if(this.player.state != PLAYER_STATE.BIG)
	 		this.playerDead();  
    },
    
    // Object actions
    addObject : function(type){
	    var nObj = new flyingElm(type, this.lvlConfig);
	    this.addChild(nObj, 2);
    },
    removeObject : function(obj){
	    this.removeChild(obj);
    },
    
    /*** Private functions ****/
    // Returns a random integer between min (included) and max (included)
	getRandomInt: function(min, max) {
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}
});

var GameScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new GameLayer();
        this.addChild(layer);
    }
});

