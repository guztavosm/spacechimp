var ScoreLayer = cc.Layer.extend({
	// Properties
	lifeLabel : null,
	lifeSprite : null,
	scoreLabel : null,
	gasBar : null,
	
	// Functions
	ctor:function () {
        this._super();
        
        var size = cc.winSize;
        
        var scoreY = size.height - 30;
        
        // Sprite mono de vidas
		this.lifeSprite = new cc.Sprite(res.vida1_png);
        this.lifeSprite.x = 40;
        this.lifeSprite.y = scoreY;
        this.lifeSprite.setScale(0.08); // Escala provisional 0.06
        // agrega el sprite al layer
        this.addChild(this.lifeSprite, 1);
        
        // Label de las vidas
        this.lifeLabel = new cc.LabelTTF("X 5", "pepsi", 26);
        this.lifeLabel.x = this.lifeSprite.x + (this.lifeSprite.getBoundingBox().width/2) + 5; // Posicion relativa junto al sprite del mono
        this.lifeLabel.y = scoreY - 5;
        this.lifeLabel.color = cc.color("#00ff4c");
        this.lifeLabel.setAnchorPoint(0, 0.5);
        // agrega el label al layer
        this.addChild(this.lifeLabel, 2);
        
		// Label del score
		this.scoreLabel = new cc.LabelTTF("0000", "pepsi", 26);
        this.scoreLabel.x = size.width - 30;
        this.scoreLabel.y = scoreY - 5;
        this.scoreLabel.color = cc.color("#00ff4c");
        this.scoreLabel.setAnchorPoint(1, 0.5);
        // agrega el label al layer
        this.addChild(this.scoreLabel, 2);

		// Gas Bar
		this.gasBar = new gasBar();
		this.gasBar.x = this.lifeSprite.x - (this.lifeSprite.getBoundingBox().width/2) + (this.gasBar.getBoundingBox().width/2); 
		this.gasBar.y = scoreY - (this.lifeSprite.getBoundingBox().height/2) - 10; // Posicion bajo el sprite del mono
		// Agrega la gasBar al layer
		this.addChild(this.gasBar, 2);
		
        return 1;
    },
    // Score Label functions
    setScore : function(score){
	    var strscore = this.zeroFill(score, 4);
	    this.scoreLabel.setString(strscore);
    },
    
    // Life Label functions
    setLifes : function(lifes){
	  this.lifeLabel.setString("X "+lifes.toString());  
    },
    
    // Gasbar sprite functions
    setGasLevel : function(level){
		this.gasBar.replaceGas(level);  
    },
    gasRestart : function(){
	    this.gasBar.restart();
    },
    
    /*** Private functions ****/
    // Returns a string with the number filled with zeros
    zeroFill : function( number, width ){
		width -= number.toString().length;
		if ( width > 0 )
		{
		return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
		}
		return number + ""; // always return a string
	}
});