var Player = cc.Sprite.extend({
	// Properties
	state  : PLAYER_STATE.NORMAL,
	gravity: CONST.GRAVITY,
	thrust: CONST.THRUST,
	gasLevel: 7,
	
	ySpeed:  0,
	engineOn: false,
	initPosition: null,
	
	actionActiva: null,
	
	// Functions
	ctor: function(){
		this._super();
		this.initWithFile(res.player1_png)
		this.initPosition = cc.p((this.getBoundingBox().width /2) + 50, cc.winSize.height/2);
		this.setScale(0.35);
		this.setPosition(this.initPosition);
		
		this.normal();
	},
	
	// Timing functions
	updateY: function(){
		var size = cc.winSize;
		var mySize = this.getChimpBox(); // Get my actual size = size * scale - fire height
		var totalSize = this.getBoundingBox();
		
		if(this.engineOn){
			this.ySpeed += this.thrust;
		}
		
		var maxY = size.height - (totalSize.height / 2);
		var minY = totalSize.height - mySize.height;

		if(this.getPosition().y + this.ySpeed <= maxY && this.getPosition().y + this.ySpeed >= minY)
			this.setPositionY(this.getPosition().y+this.ySpeed);
		else{
			if(this.getPosition().y >= size.height/2)
				this.setPositionY(maxY);
			else
				this.setPositionY(minY)
			this.ySpeed = 0;
		}
		
		this.ySpeed += this.gravity;
	},
	
	// Utility functions
	getChimpBox: function(){
		var mySize = this.getBoundingBox();
		
		var yFuego = mySize.height * 0.25;
		mySize.height -= yFuego;
		mySize.y += yFuego;
		return mySize;
	},
	
	// GasLevel functions
	reduceGas : function(){
		this.gasLevel--;
		if(this.gasLevel <= CONST.LOWGASLEVEL && this.state == PLAYER_STATE.NORMAL)
			this.lowGas();
			
		return this.gasLevel;
	},
	increaseGas : function(){
		if(this.gasLevel < CONST.MAXGASLEVEL){
			this.gasLevel++;
			
			if(this.gasLevel > CONST.LOWGASLEVEL && this.state == PLAYER_STATE.NORMAL)
				this.normal();	
		}
		return this.gasLevel; // Gas full
	},
	
	// Actions
	big: function(){
		this.stopAction(this.actionActiva);
		this.state = PLAYER_STATE.BIG;
		var rect = new cc.rect(0,0, 360, 360);
		
		var animBig = [
	        new cc.SpriteFrame(res.big1_png, rect),
	        new cc.SpriteFrame(res.big2_png, rect),
	        new cc.SpriteFrame(res.big3_png, rect),
	        new cc.SpriteFrame(res.big2_png, rect)
        ];
        
        var animBigA = new cc.Animation(animBig, 0.12);
        var animate = new cc.Animate(animBigA);
        animate.retain();
        this.actionActiva = this.runAction(animate.repeatForever());
        
        var scaleAnim = new cc.ScaleTo(1, 1.2);
        this.runAction(scaleAnim);
	},
	die: function(){
		//Restart functions
	    this.revive();
	},
	lowGas: function(){
		this.stopAction(this.actionActiva);
		var rect = new cc.rect(0,0, 360, 360);
		
		var animLowGas = [
	        new cc.SpriteFrame(res.player4_png, rect),
	        new cc.SpriteFrame(res.player5_png, rect)
        ];
        
        var animLowGasA = new cc.Animation(animLowGas, 0.12);
        var animate = new cc.Animate(animLowGasA);
        animate.retain();
        this.actionActiva = this.runAction(animate.repeatForever());
		
		
	},
	normal: function(){
		this.stopAction(this.actionActiva);
		if(this.state != PLAYER_STATE.INVUNERABLE)
			this.state = PLAYER_STATE.NORMAL;
		var rect = new cc.rect(0,0, 360, 360);
		
		var animNormal = [
	        new cc.SpriteFrame(res.player1_png, rect),
	        new cc.SpriteFrame(res.player2_png, rect),
	        new cc.SpriteFrame(res.player3_png, rect),
	        new cc.SpriteFrame(res.player2_png, rect)
        ];
        
        var animNormalA = new cc.Animation(animNormal, 0.12);
        var animate = new cc.Animate(animNormalA);
        animate.retain();
        this.actionActiva = this.runAction(animate.repeatForever());
	},
	revive: function(){
		var self = this;
		this.setPosition(this.initPosition);
		this.gasLevel = 7;
		this.state = PLAYER_STATE.INVUNERABLE;
		
		var blinkAction = new cc.Blink(3, 12);
		// Change state
		var changeState = new cc.CallFunc(function(){
			self.state = PLAYER_STATE.NORMAL;
		});
		var actionSequence = new cc.Sequence(blinkAction, changeState);
		
		this.normal();
		this.runAction(actionSequence);
	}
});