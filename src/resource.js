var folder = "";
var fontFolder = "";

if (!cc.sys.isNative)
{
    folder = "res/mediumRes/";
    fontFolder = "res/fonts/"
}

var res = {
    vida1_png : folder + "vida-1.png",
    vida2_png : folder + "vida-2.png",
    vida3_png : folder + "vida-3.png",
    // Background
    bg1_png : folder + "bg1.png",
    bg2_png : folder + "bg2.png",
    bg3_png : folder + "bg3.png",
    bg4_png : folder + "bg4.png",
    
    // Sprites
    player1_png : folder + "player-1.png",
    player2_png : folder + "player-2.png",
    player3_png : folder + "player-3.png",
    player4_png : folder + "player-4.png",
    player5_png : folder + "player-5.png",
    
    big1_png : folder + "big-1.png",
    big2_png : folder + "big-2.png",
    big3_png : folder + "big-3.png",
    
    gas1_png : folder + "gas-1.png",
    gas2_png : folder + "gas-2.png",
    gas3_png : folder + "gas-3.png",
    gas4_png : folder + "gas-4.png",
    gas5_png : folder + "gas-5.png",
    gas6_png : folder + "gas-6.png",
    gas7_png : folder + "gas-7.png",
    
    met_png : folder + "met.png",
    metverde_png : folder + "met-verde.png",
    metamarillo_png : folder + "met-amarillo.png",
    metrojo_png : folder + "met-rojo.png",
    vida_png : folder + "vida-juego.png",
    gasolina_png: folder + "gasolina.png",
    
    // Fuentes
    pepsi_ttf : fontFolder + "pepsi.ttf",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
