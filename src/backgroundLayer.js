var BackgroundLayer = cc.Layer.extend({
	// Properties
	actualBg: 1,
	background : null,
	auxBg: null,
	bgSpeed: CONST.BGSPEED,
	
	// Functiones
	ctor:function () {
        this._super();
        
        this.background = new cc.Sprite(res.bg1_png);
        this.auxBg = new cc.Sprite(res.bg1_png);  
        
        this.addChild(this.background);
        this.addChild(this.auxBg);

        this.initPosition();
        //this.schedule(this.change, 10);
        
        return 1; 
    },
    initPosition : function(){
	    var size = cc.winSize;
	    
	    this.background.setPosition(size.width / 2, size.height / 2);
	    this.auxBg.setPosition((size.width / 2) + this.background.width, size.height / 2);
	    this.auxBg.setFlippedX(true); 
    },
    changeBg : function(bg){
	    switch(bg){
		    case 1:
		    	this.background.initWithFile(res.bg1_png);
		    	this.auxBg.initWithFile(res.bg1_png);
		    	break;
		    case 2:
				this.background.initWithFile(res.bg2_png);
				this.auxBg.initWithFile(res.bg2_png);
		    	break;
		    case 3:
		    	this.background.initWithFile(res.bg3_png);
		    	this.auxBg.initWithFile(res.bg3_png);
		    	break;
		    case 4:
		    	this.background.initWithFile(res.bg4_png);
		    	this.auxBg.initWithFile(res.bg4_png);
		    	break;
	    }
	    this.initPosition();
    },
    change: function(){
	    this.actualBg++;
	    
	    if(this.actualBg > 4) 
	    	this.actualBg = 1;
	    	
	    this.changeBg(this.actualBg);
		cc.log("Fondo cambiado");
		  
    },
    scroll : function(){
	    this.background.x -= this.bgSpeed;
	    this.auxBg.x -= this.bgSpeed;
	    
	    if(this.background.x <= (this.background.width/2)*(-1))
		    this.background.x = this.auxBg.x + (this.auxBg.width / 2) + (this.background.width / 2);
		    
		if(this.auxBg.x <= (this.auxBg.width/2)*(-1))
		    this.auxBg.x = this.background.x + (this.auxBg.width / 2) + (this.background.width / 2);
    } 
});